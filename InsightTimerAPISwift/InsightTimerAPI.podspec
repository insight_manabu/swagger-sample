Pod::Spec.new do |s|
  s.name = 'InsightTimerAPI'
  s.ios.deployment_target = '9.0'
  s.osx.deployment_target = '10.11'
  s.tvos.deployment_target = '9.0'
  s.version = '0.0.1'
  s.source = { :git => 'git@github.com:swagger-api/swagger-mustache.git', :tag => 'v1.0.0' }
  s.authors = 'Insight Network, Inc'
  s.license = 'Proprietary'
  s.homepage = 'https://insighttimer.com/'
  s.summary = 'Insight Timer API Library'
  s.source_files = 'InsightTimerAPI/Classes/**/*.swift'
  s.dependency 'Alamofire', '~> 4.5.0'
end
