part of swagger.api;

class Publication {
  
  String id = null;
  

  Publisher publisher = null;
  

  String contentType = null;
  //enum contentTypeEnum {  DAILY_INSIGHT,  LIBRARY_ITEM,  };
  Publication();

  @override
  String toString() {
    return 'Publication[id=$id, publisher=$publisher, contentType=$contentType, ]';
  }

  Publication.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    id =
        json['id']
    ;
    publisher =
      
      
      new Publisher.fromJson(json['publisher'])
;
    contentType =
        json['contentType']
    ;
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'publisher': publisher,
      'contentType': contentType
     };
  }

  static List<Publication> listFromJson(List<dynamic> json) {
    return json == null ? new List<Publication>() : json.map((value) => new Publication.fromJson(value)).toList();
  }

  static Map<String, Publication> mapFromJson(Map<String, Map<String, dynamic>> json) {
    var map = new Map<String, Publication>();
    if (json != null && json.length > 0) {
      json.forEach((String key, Map<String, dynamic> value) => map[key] = new Publication.fromJson(value));
    }
    return map;
  }
}

