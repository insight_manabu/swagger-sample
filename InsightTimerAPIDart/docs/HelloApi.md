# swagger.api.HelloApi

## Load the API package
```dart
import 'package:swagger/api.dart';
```

All URIs are relative to *https://insight-timer-api.firebaseapp.com/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**apiDailyInsightGetRequestDailyinsightsGet**](HelloApi.md#apiDailyInsightGetRequestDailyinsightsGet) | **GET** /apiDailyInsightGet/request/dailyinsights | 


# **apiDailyInsightGetRequestDailyinsightsGet**
> apiDailyInsightGetRequestDailyinsightsGet(offset)



### Example 
```dart
import 'package:swagger/api.dart';

var api_instance = new HelloApi();
var offset = 789; // int | This is the offset 😄

try { 
    api_instance.apiDailyInsightGetRequestDailyinsightsGet(offset);
} catch (e) {
    print("Exception when calling HelloApi->apiDailyInsightGetRequestDailyinsightsGet: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **offset** | **int**| This is the offset 😄 | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

