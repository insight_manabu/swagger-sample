# swagger.api.DefaultApi

## Load the API package
```dart
import 'package:swagger/api.dart';
```

All URIs are relative to *https://insight-timer-api.firebaseapp.com/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**apiAdminGlobalStatsRequestStatsGet**](DefaultApi.md#apiAdminGlobalStatsRequestStatsGet) | **GET** /apiAdminGlobalStats/request/stats | 
[**apiDailyInsightGetRequestDailyinsightsGet**](DefaultApi.md#apiDailyInsightGetRequestDailyinsightsGet) | **GET** /apiDailyInsightGet/request/dailyinsights | 


# **apiAdminGlobalStatsRequestStatsGet**
> apiAdminGlobalStatsRequestStatsGet()



### Example 
```dart
import 'package:swagger/api.dart';

var api_instance = new DefaultApi();

try { 
    api_instance.apiAdminGlobalStatsRequestStatsGet();
} catch (e) {
    print("Exception when calling DefaultApi->apiAdminGlobalStatsRequestStatsGet: $e\n");
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **apiDailyInsightGetRequestDailyinsightsGet**
> List<Publication> apiDailyInsightGetRequestDailyinsightsGet(offset, limit)



### Example 
```dart
import 'package:swagger/api.dart';

var api_instance = new DefaultApi();
var offset = 789; // int | This is the offset 😄
var limit = 789; // int | This is the limit 😃

try { 
    var result = api_instance.apiDailyInsightGetRequestDailyinsightsGet(offset, limit);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->apiDailyInsightGetRequestDailyinsightsGet: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **offset** | **int**| This is the offset 😄 | [optional] 
 **limit** | **int**| This is the limit 😃 | [optional] 

### Return type

[**List<Publication>**](Publication.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

