#!/usr/bin/env bash

SWAGGER_EXECUTABLE="swagger-codegen-cli.jar"

if [ ! -f "$SWAGGER_EXECUTABLE" ]; then
    wget http://central.maven.org/maven2/io/swagger/swagger-codegen-cli/2.4.5/swagger-codegen-cli-2.4.5.jar -O $SWAGGER_EXECUTABLE
fi

java -jar $SWAGGER_EXECUTABLE generate \
    -i ./insight-timer-api.yaml \
    -l swift4 \
    -o ./InsightTimerAPISwift \
    -c ./apiclient-generator-settings-ios.json

java -jar $SWAGGER_EXECUTABLE generate \
    -i ./insight-timer-api.yaml \
    -l dart \
    -o ./InsightTimerAPIDart
